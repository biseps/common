
# In[]:

import numpy as np

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


# In[]:

def func(x, a, b, c):
    return a * np.exp(-b * x) + c

x = np.linspace(0,4,50)
y = func(x, a=2.5, b=1.3, c=0.5)
yn = y + 0.2*np.random.normal(size=len(x))

popt, pcov = curve_fit(func, x, yn)

# And then if you want to plot, you could do:

plt.figure()

plt.plot(x, yn, 'ko', label="Original Noised Data")

plt.plot(x, func(x, *popt), 'r-', label="Fitted Curve")

plt.legend()
plt.show()              

# In[]:

