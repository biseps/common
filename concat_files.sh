#!/bin/bash


# Need a minimum of 4 arguments
if [ "$#" -ne 3 ];
then
    echo "Usage:"
    echo "\$1 : start folder number"
    echo "\$2 : end folder number"
    echo "\$3 : filename to concatonate"
    exit 1
fi


# parse arguments
start="$1"
end="$2"
file="$3"


# always set the base directory
# to current working directory
BASE_DIR="$PWD"

echo "start: ${start}"
echo "end: ${end}"


for (( i=$start; i<=$end; i++ ))
do
    # concatonate totals files
    echo "appending $file $i"
    cat  "$BASE_DIR/$i/$file" >> "$BASE_DIR/$file"

done


