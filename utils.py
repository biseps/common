# In[]:

import os, sys, time, shutil
# sys.path.append('../biseps2_plot/')

import glob

# set matplotlib backend
#matplotlib.use('Agg')
#matplotlib.use('Qt4Agg')

# once backend is sorted, then import pyplot
import matplotlib
import matplotlib.pyplot as plt
# plt.rcParams['axes.grid'] = True

import sqlite3
from cStringIO import StringIO
import logging

from astropy.io import ascii as ascii_file



def filter_list(input_list, remove):
    """remove specific element from list
    """
    return [value 
            for value in input_list 
            if value != remove]


def get_subdirs(root_dir):
    """list all immediate sub-directories, 
        (i.e. to a max depth of 1)

    :root_dir: starting directory
    :returns: list of sub-directories

    """
    root, subdirs, files = os.walk(root_dir).next()

    return subdirs



def load_files(glob_pattern, headers):
    """@todo: Docstring for load_files.

    :glob_pattern: regex glob pattern to identify files to load
    :headers: column names for the file data
    :returns: astropy table object

    """

    # summary_data  = []
    file_data  = {}

    file_list =  glob.glob(glob_pattern)
    file_list.sort()
    print file_list

    for file_name in file_list:
        print "reading file: " + file_name
        file_data[file_name] = ascii_file.read(file_name, names=headers)
        # file_data.append(ascii_file.read(file_name, names=headers))

    return file_data


def set_logging_config(logging, log_level=logging.INFO, filebase='debug'):
    """ Set default options for the python logging module

    :logging: an instance of the python logging module
    :log_level: severity of messages
    :filename: (optional) name for logging file
    """

    # put logging files 
    # in the home directory
    log_folder  = os.path.expanduser('~/')

    filename    = '.'.join([filebase, 
                            time.strftime('%b-%d'), 
                            time.strftime('%H-%M-%S'), 
                            'log'])

    # filemode='w', 

    logging.basicConfig(filename=os.path.join(log_folder, filename), 
                        level=log_level, 
                        format='%(asctime)s %(message)s', 
                        datefmt='%a %d-%b %H:%M:%S')



def sqlite_to_memory(disk_file):
    """Load an sqlite database into memory

    :disk_file: path to sqlite db
    :returns:   sqlite database memory instance

    """
    disk_db  = sqlite3.connect(disk_file)
    tempfile = StringIO()

    for line in disk_db.iterdump():
        tempfile.write('%s\n' % line)

    disk_db.close()
    tempfile.seek(0)

    # Create a database in memory and import from tempfile
    mem_db = sqlite3.connect(":memory:")
    mem_db.cursor().executescript(tempfile.read())
    mem_db.commit()

    return mem_db



def get_runtime():
    return time.strftime('%Y%m%d-%H_%M_%S')



def make_folder(folder_name):

    if not os.path.exists(folder_name):
        os.mkdir(folder_name)


def rotate_folder(folder_name, run_number=0):

    # first backup folder 
    # if it exists already
    if os.path.exists(folder_name):
        # remove any trailing slashes...
        # it messes up copytree if you have a slash
        folder_name = folder_name.rstrip(os.sep)
        shutil.copytree(folder_name, folder_name + '_' + str(get_runtime()))

        # delete folder
        print "about to delete existing " + folder_name
        shutil.rmtree(folder_name)

    # now (re)create folder again!
    os.mkdir(folder_name)



def save_plot(fig, folder, filename, img_format='jpg', show=True):
    """@todo: Docstring for save_plot.

    :folder: @todo
    :filename: @todo
    :format: @todo
    :returns: @todo

    """
    fig.suptitle(folder + ' - ' + filename)
    fig.savefig(os.path.join(folder, filename + '.' + img_format), format=img_format)

    if show:
        plt.show()

    plt.close(fig)



def plot_color_color(color1, color2, titles, folder, filename, combined=False):
    """@todo: Docstring for plot_color_color.

    :color1: @todo
    :color2: @todo
    :titles: @todo
    :folder: @todo
    :filename: @todo
    :combined: @todo
    :returns: @todo

    """

    fig = plt.figure(figsize=(10, 15))

    if combined:
        for (i, title) in enumerate(titles):
            ax = plt.subplot(111)
            ax.set_title(filename)
            ax.grid(True, which='minor')
            ax.set_xlabel('g-r', fontsize=13)
            ax.set_ylabel('r-i', fontsize=13)
            ax.scatter(color1[i], color2[i], s=3, lw=0, alpha=0.7)

    else:
        for (i, title) in enumerate(titles):
            ax = plt.subplot(221 + i)

            ax.set_title('%s' % title)
            ax.grid(True, which='minor')

            ax.set_xlabel('g-r', fontsize=13)
            ax.set_ylabel('r-i', fontsize=13)

            ax.scatter(color1[i], color2[i], s=3, lw=0, alpha=0.7)


    fig.suptitle(folder + ' - ' + filename)
    save_plot(fig, folder, filename)






# In[]:

def plot_field_stars(lon, lat, titles, folder, filename):
    """@todo: Docstring for plot_field_stars.

    :data: @todo
    :titles: @todo
    :folder: @todo
    :filename: @todo
    :returns: @todo

    """
    fig = plt.figure(figsize=(20, 20))

    ax = plt.subplot(111)
    ax.set_title(folder + ' field')
    ax.grid(True, which='minor')
    ax.set_xlabel('long', fontsize=13)
    ax.set_ylabel('lat', fontsize=13)

    kwargs_stars = dict(marker='o', s=5, color='red', lw=1, alpha=0.6)
    kwargs_binaries = dict(marker='+', s=95, color='gray', lw=1, alpha=0.7)

    # kwargs_star_old = dict(marker='o', s=5, color='red', lw=1, alpha=0.6)
    # kwargs_star_young = dict(marker='o', s=5, color='green', lw=1, alpha=0.6)
    # kwargs_binary_old = dict(marker='+', s=95, color='gray', lw=1, alpha=0.7)
    # kwargs_binary_young = dict(marker='+', s=95, color='blue', lw=1, alpha=0.7)

    # ax.scatter(longs[0], lats[0], label=titles[0],  **kwargs_star_old)
    # ax.scatter(longs[1], lats[1], label=titles[1],  **kwargs_star_young)
    # ax.scatter(longs[2], lats[2], label=titles[2],  **kwargs_binary_old)
    # ax.scatter(longs[3], lats[3], label=titles[3],  **kwargs_binary_young)

    # for i, title in enumerate(titles):
        # ppl.scatter(ax, lon[i], lat[i], label=title)

    # ppl.legend(ax, loc='upper right', ncol=2)

    ax.minorticks_on()
    ax.grid(True, which='both', color='grey', linewidth=1)

    # legend with some customizations.
    # legend = ax.legend(loc='upper right', shadow=False)

    save_plot(fig, folder, filename)



def plot_field_stars_binaries(lon, lat, titles, folder, filename):
    """@todo: Assume just plotting stars and binaries!

    :data: @todo
    :titles: @todo
    :folder: @todo
    :filename: @todo
    :returns: @todo

    """
    fig = plt.figure(figsize=(20, 20))

    ax = plt.subplot(111)
    ax.set_title(folder + ' ' + filename)
    ax.grid(True, which='minor')
    ax.set_xlabel('long', fontsize=13)
    ax.set_ylabel('lat', fontsize=13)

    kwargs_stars = dict(marker='o', s=5, color='red', lw=1, alpha=0.6)
    kwargs_binaries = dict(marker='+', s=55, color='green', lw=1, alpha=0.7)

    ax.scatter(lon[0], lat[0], label=titles[0],  **kwargs_stars)
    ax.scatter(lon[1], lat[1], label=titles[1],  **kwargs_binaries)

    ax.minorticks_on()
    ax.grid(True, which='both', color='grey', linewidth=1)

    # legend with some customizations.
    legend = ax.legend(loc='upper right', shadow=False)

    save_plot(fig, folder, filename)




# In[]:

def passband_histogram(data, titles, folder, filename, do_normed=True ):
    """@todo: Docstring for passband_histogram.

    :data: @todo
    :titles: @todo
    :folder: @todo
    :filename: @todo
    :do_normed: @todo
    :returns: @todo

    """
    fig = plt.figure(figsize=(10, 15))

    binsize=40
    bartype='step'

    for (i, star_type) in enumerate(titles):
        ax = plt.subplot(221 + i)

        ax.set_title('%s' % star_type)
        ax.grid(True, which='minor')

        h1 = ax.hist(data[i]['u'], bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.5, label='u')
        h2 = ax.hist(data[i]['g'], bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='green', alpha=0.5, label='g')
        h3 = ax.hist(data[i]['r'], bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='red', alpha=0.5, label='r')
        h4 = ax.hist(data[i]['i'], bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='yellow', alpha=0.5, label='i')
        h5 = ax.hist(data[i]['z'], bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='gray', alpha=0.5, label='z')

        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, labels)

        ax.set_xlabel('magnitude', fontsize=13)
        ax.set_ylabel('count', fontsize=13)
        # ax.set_ylim([0,y_axis_limit])

    save_plot(fig, folder, filename)



def highlight_neighbours(target, neighbours, dist):
    """@todo: Docstring for highlight_neighbours.

    :target: @todo
    :neighbours: @todo
    :returns: @todo

    """
    labels = ['neighbour{0} ({1:.3f})'.format(i+1, dist[i]) 
                for i in range(len(neighbours))]


    # draw arrow to target
    x = target['long']
    y = target['lat']

    plt.annotate(
        'star',
        xy = (x, y), xytext = (-20, 20),
        textcoords = 'offset points', ha = 'right', va = 'bottom',
        bbox = dict(boxstyle = 'round,pad=0.3', fc = 'red', alpha = 0.5),
        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))



    # draw  arrows from neighbours
    for neighbour in neighbours:

        x = neighbour['long']
        y = neighbour['lat']

        random_offset_y = y + (i * randint(1,9))

        plt.annotate(
            labels[i],
            xy = (x, y), xytext = (100, random_offset_y),
            textcoords = 'offset points', ha = 'left', va = 'top',
            bbox = dict(boxstyle = 'round,pad=0.1', fc = 'green', alpha = 0.3),
            arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))




