#!/bin/bash


# Need 2 arguments
if [ "$#" -ne 2 ];
then
    echo "Usage:"
    echo "\$1 : start folder number"
    echo "\$2 : end folder number"
    exit 1
fi

# parse command-line args
start="$1"
end="$2"

# always set the base directory
# to current working directory
BASE_DIR="$PWD"


echo "base dir: $BASE_DIR"
echo "start: ${start}"
echo "end: ${end}"


for (( i=$start; i<=$end; i++ ))
do
    # Remove Directory
    echo "Deleting $i"
    rm -Rf "$BASE_DIR/$i" 
done
