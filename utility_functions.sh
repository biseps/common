#!/bin/bash

# collection of useful bash functions


get_date() 
{ 
    echo $(date +'%b-%d %a')
}



get_time() 
{ 
    echo $(date +%T)
}


assert_is_number() 
{ 
    test "$1" && printf '%f' "$1" >/dev/null; 

    if [[ $? -ne 0 ]] ;then 
        echo "fatal: value must be numeric."
        echo "value: $1"
        exit 1
    fi
}


insert_log() 
{
    COMMENT="$1"
    OUTPUT_DIR="$2"

    TODAY=$(get_date)
    TIME=$(get_time)

    printf "\n%s (%s) - %s - %s\n"  "$TODAY" "$TIME"  "..."${OUTPUT_DIR:(-25)}  "$COMMENT"
}



echo_value() 
{
    LABEL="$1"
    VALUE="$2"  # can be omitted

    printf "\n\n$LABEL: 	\n%s\n"  "$VALUE"
}



debug_msg() 
{
    MSG="$1"

    TODAY=$(get_date)
    TIME=$(get_time)

    printf "\n%s (%s) - %s \n"  "$TODAY" "$TIME"  "$MSG"
}




echo_msg() 
{
    printf "\n\n%s\n"  "$1"
}



echo_job_time() 
{

    TODAY=$(get_date)
    TIME=$(get_time)

    printf "%s, %s\n"  "$TODAY"  "$TIME"
}



assert_not_null() 
{
    name="$1"
    value=$2

    if [ -z "$value" ]; then
        echo "fatal: ${name} is null (zero length)"
        exit 1
    fi
}



assert_file_exists() 
{
    description="$1"
    path="$2"

    # echo "in assert_file_exists"
    # echo "$description"
    # echo "$path"

    if [ ! -f "$path" ]; then
        printf "\nError: File '$description' does not exist. Path supplied: $path\n"
        exit 1
    fi
}



assert_folder_exists() 
{
    description="$1"
    path="$2"

    if [ ! -d "$path" ]; then
        printf "\nError: Folder '$description' does not exist. Path supplied: $path\n"
        exit 1
    fi
}



increment_folder() 
{
    # Purpose: 
    # return the next available directory
    # in a numbered sequence of directories

    # Example: We have
    # log1
    # log2
    # log3 ..etc..
    # We want this function to return "log4"


    BASE_FOLDER="$1"

    num=1

    # keep looping till we find
    # the next available slot
    while [ -d "$BASE_FOLDER$num" ]; do
        let num++
    done

    echo "$BASE_FOLDER$num"
}



set_python_env() 
{
    # Need 1 or 2 arguments
    if [ "$#" -ne 1 -a "$#" -ne 2 ];
    then
        echo "set_python_env usage:"
        echo "\$1 : Python Distrubtion"
        echo "\$2 : (optional) location of extra code for PYTHONPATH"
        exit 1
    fi


    # 1st argument - convert to uppercase
    DISTRO="$(echo "$1" | tr '[:lower:]' '[:upper:]')"


    # 2nd argument - optional
    if [ -n "$2" ] 
    then
        EXTRA_CODE_PATHS="$2"
    fi


    # location of Anaconda python distribution
    anaconda_path="/padata/beta/users/pmr257/anaconda"


    # Use which python distribution?
    case $DISTRO in

        "ENTHOUGHT")
            PYTHONBIN="/csoft/epd-7.2-64/bin/python"
            PYTHONPATH="/csoft/epd-7.2-64/lib/python2.7/site-packages"
            ;;

        "ANACONDA")
            PYTHONBIN="$anaconda_path/bin/python"
            PYTHONPATH="$anaconda_path/lib/python2.7/site-packages"
            ;;

        *)
            echo_value "Unknown Python Distrubtion:" ${DISTRO}
            exit 1
            ;;
    esac


    if [ -n "$EXTRA_CODE_PATHS" ]
    then
        # add extra locations to PYTHONPATH 
        PYTHONPATH="$PYTHONPATH:$EXTRA_CODE_PATHS"
        PYTHONPATH="$PYTHONPATH:$EXTRA_CODE_PATHS/mag"
        PYTHONPATH="$PYTHONPATH:$EXTRA_CODE_PATHS/plot"
    fi


    # export environment variables
    export PYTHONPATH
    export PYTHONBIN


    # let user know
    echo_value  "PYTHONBIN"   "$PYTHONBIN"
    echo_value  "PYTHONPATH"  "$PYTHONPATH"
}




checkout_branch()
{
    branch_name="$1"
    folder="$2"

    echo_msg ${folder}

    cd ${folder}
    git checkout ${branch_name}
    cd - > /dev/null 
}
 
 


show_git_status()
{
    GIT_DIR="$1"
    printf '\n\nGIT REPOSITORY: %s\n'  "$(basename $GIT_DIR)"


    if [ ! -d "$GIT_DIR" ]; then

        # dodgy input parameter
        echo_value 'error: directory does not exist'  "$GIT_DIR"

    else

        # must actually "cd" into directory,
        # so that git commands to work
        cd "$GIT_DIR" > /dev/null

        # git rev-parse --short HEAD
        # cat .git/HEAD
        git log --date=short --pretty=format:'%ad  %h  %s' -1
        printf "\n"
        git branch --no-color
        printf "\n"
        git status --short --porcelain 

        # return to previuos directory
        cd - > /dev/null

    fi

}



