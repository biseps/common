#!/bin/bash


# Can take 2 or 3 arguments
if [ "$#" -ne 2 -a "$#" -ne 3 ];
then
    echo "Usage:"
    echo "\$1 : start folder number"
    echo "\$2 : end folder number"
    echo "\$3 : (optional) file to copy into each folder"
    exit 1
fi


# parse command-line args
start="$1"
end="$2"

# always set the base directory
# to current working directory
BASE_DIR="$PWD"

# file is optional
[ -n "$3" ] && file="$3"


echo "base dir: $BASE_DIR"
echo "start: ${start}"
echo "end: ${end}"


for (( i=$start; i<=$end; i++ ))
do
    # Create Directory
    echo "Creating $i"
    mkdir "$BASE_DIR/$i" 


    # if specified, copy a
    # file into each new folder
    if [ -n "$file" ]; 
    then
        cp $file $i
    fi
done
