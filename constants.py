#Constants
import numpy as np
g=6.67384*10**(-11)
rsun=6.995*10**8
msun=1.9891*10**30
au=149.6*10**9
teffsun=5777.0
year=365*24*60*60.0
gn=g*5.86676*10**(18)
rearth=6378.1*10**3
numRows=1070
numCols=1132
root='/padata/beta/users/rfarmer/'
prfDir=root+'tables/kepler/prf/'
fovDir=root+'tables/kepler/fov/'
#prfDir='/home/Rob/Desktop/biseps/tables/kepler/prf/'
#fovDir='/home/Rob/Desktop/biseps/tables/kepler/fov/'
#ffiDir='./'
#CCD season used
season=3
#Number of semimajor axes to calc
numA=3


#int time
tint=6.02
#read time 
readTime=0.52
#number of frames in 30 mins
nfr=3575.0
#Read noise?
nr=np.array([87.6,103.3,116.4,111.0,80.5,92.0,149.0,108.1,88.6,73.9,83.2,84.2,84.0,78.8,83.1,81.8,85.4,81.0,85.9,85.6,80.2,95.0,83.2,86.7,90.0,116.4,89.3,82.5,80.7,99.5,83.0,84.6,84.3,88.1,92.5,120.5,79.5,80.9,87.5,121.1,84.2,84.8,81.3,126.9,88.2,85.5,90.4,112.0,85.7,83.0,81.5,103.8,87.4,109.2,90.5,77.1,86.2,120.6,88.1,93.7,79.4,109.4,86.2,85.5,85.6,89.6,90.3,80.0,81.4,81.0,83.2,87.1,79.1,79.4,82.3,84.6,81.3,83.9,83.1,84.0,96.3,97.0,139.0,99.5])
#num of electrons for 12th mag star per second
f12=2.1*10**5
#num of cadences per short cadences
numShort=9.0
#Running per candence (long cadence)
numCand=30.0
#Number of integrations in 1 long cadence
numInt=numCand*numShort
#read noise 
readNoiseSq=(nr)**2
#readNoiseSq=np.zeros(np.shape(nr))
#Kepler instrument handbook
#e- per pixel
wellDepth=np.array([1.03,1.04,1.08,1.05,1.18,1.18,1.09,1.05,1.01,0.947,1.1,1.13,1.07,1.02,1.08,1.05,1.02,1.04,1.02,1.03,1.07,1.06,1.02,1.05,1.11,1.13,1.09,1.05,1.13,1.15,1.10,1.14,1.05,1.02,1.02,1.07,1.1,1.05,1.08,1.1,1.12,1.13,1.04,1.05,1.04,1.02,1.0,1.0,1.12,1.15,1.0,1.0,1.1,1.14,1.17,1.15,1.09,1.06,1.08,1.09,1.03,1.03,1.04,1.06,1.01,1.02,1.06,1.01,1.16,1.14,1.13,1.13,1.13,1.14,1.19,1.16,1.07,1.05,1.15,1.13,1.0,1.0,1.14,1.12])
wellDepth=wellDepth*10**6

#quantisation bits
bits=14

#zodical noise
#zod=np.zeros([numRows,numCols])
zod=10**((19.0)/-2.5)
#zod=0.0

#flux to electrons
f2e=10**(0.4*12.0)*f12*tint


#See Selecting pixels for Kepler downlink Proc. SPIE 7740, 77401D (2010) (bryson et al)
#file PSISDG_7740_1_7401D_1.pdf
quantErr=np.sqrt(1.0/12.0)*wellDepth/(2**(bits-1))
quantErrSq=(quantErr)**2
#quantErrSq=np.zeros(np.shape(quantErr))

#Charge transfer efficney
ctePar=0.99993

cteSer=0.99995
		